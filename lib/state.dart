
import 'home_page.dart';

class OurKurwaState {
  String username = 'aaa';
  List<Claim> claims = [];

  OurKurwaState({this.username, this.claims});
}

OurKurwaState reducer(OurKurwaState previousState, dynamic action) {
  switch (action.type) {
    case OurActions.loadClaims:
      return new OurKurwaState(username: previousState.username, claims: action.claims);
    case OurActions.change:
      return new OurKurwaState(username: action.value, claims: previousState.claims);
  }
  return previousState;
}

enum OurActions {
  change,
  loadClaims
}

class UpdateUsernameAction {
  final type = OurActions.change;
  final String value;
  UpdateUsernameAction({this.value});
}

class LoadClaimsAction {
  final type = OurActions.loadClaims;
  final List<Claim> claims;
  LoadClaimsAction({this.claims});
}