import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class MapViewPage extends StatelessWidget {
  static String tag = 'map-view';

  @override
  Widget build(BuildContext context) {
    final MapViewArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(title: Text(args.address)),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () async {
            var lat = args.location.latitude;
            var lng = args.location.longitude;
            String googleUrl =
                'https://www.google.com/maps/search/?api=1&query=$lat, $lng';
            if (await canLaunch(googleUrl)) {
              await launch(googleUrl);
            } else {
              throw 'Could not open the map.';
            }
          },
          label: Text("Let's go!"),
          icon: Icon(Icons.directions_car),
        ),
        body: MapView(location: args.location));
  }
}

class MapViewArguments {
  final LatLng location;
  final String address;

  MapViewArguments(this.location, this.address);
}

class MapView extends StatefulWidget {
  const MapView({Key key, this.location}) : super(key: key);
  final LatLng location;

  @override
  State<MapView> createState() => MapViewState();
}

class MapViewState extends State<MapView> {
  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    final CameraPosition selectedLocation = CameraPosition(
      target: widget.location,
      zoom: 18,
    );

    final Set<Marker> _markers = Set();

    void addMarker() {
      _markers.add(Marker(
        position: widget.location,
      ));
    }

    return new Scaffold(
        body: GoogleMap(
      mapType: MapType.normal,
      markers: _markers,
      initialCameraPosition: selectedLocation,
      onMapCreated: (GoogleMapController controller) {
        addMarker();
        _controller.complete(controller);
      },
    ));
  }
}
