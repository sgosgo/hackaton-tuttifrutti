import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';

import 'home_page.dart';
import 'state.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  _fetchClaims(String username, Store<OurKurwaState> store) async {
    final body =
        "{ direction: '0', limit: '12', poolId: '', search: '', sort: 'EarliestDeadline', start: '0' }";
    try {

      final response = await http.post(
//          'https://0409c90f.ngrok.io/codan/api/mobile/v1/claims/list/all', headers: {HttpHeaders.contentTypeHeader: 'application/json', 'userLogin': username},
          'https://dev-lime-cw01.spcph.local/daily/codan/api/mobile/v1/claims/list/all', headers: {HttpHeaders.contentTypeHeader: 'application/json', 'userLogin': username},
          body: body);

      if (response.statusCode == 200) {
        // If server returns an OK response, parse the JSON.
        final test = json.decode(response.body);
        final result = (test["items"] as List).map((f) => new Claim.fromJson(f));
        store.dispatch(new LoadClaimsAction(claims: result.toList()));
      } else {
        // If that response was not OK, throw an error.
        throw Exception('Failed to load post');
      }
    } catch (e){
      throw Exception(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final emailController = TextEditingController(text: "test1@test.com");
    final email = TextFormField(
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: 'password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final buttonConnector = StoreConnector<OurKurwaState,VoidCallback>(
        converter: (store) {
          return () {
            var action = new UpdateUsernameAction(value: emailController.value.text);
            _fetchClaims(emailController.value.text, store);
            return store.dispatch(action);
          };
        },
        builder: (context, callback) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
              onPressed: () {
                callback();
                Navigator.of(context).pushNamed(ClaimsPage.tag);
              },
              padding: EdgeInsets.all(12),
              color: Colors.lightBlueAccent,
              child: Text('Log In', style: TextStyle(color: Colors.white)),
            ),
          );
        }
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            buttonConnector
          ],
        ),
      ),
    );
  }
}
