import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'login_page.dart';
import 'home_page.dart';
import 'camera_page.dart';
import 'painter_page.dart';
import 'state.dart';
import 'package:appcenter/appcenter.dart';
import 'package:appcenter_analytics/appcenter_analytics.dart';
import 'package:appcenter_crashes/appcenter_crashes.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'map_view_page.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = new MyHttpOverrides();
  final Store<OurKurwaState> store = Store<OurKurwaState>(reducer, initialState: new OurKurwaState(username: '', claims: []));
  runAppCenterServices();

  runApp(
      StoreProvider(
        store: store,
        child: MaterialApp(
          title: "ServicePartner",
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: LoginPage(),
          routes: <String, WidgetBuilder>{
            LoginPage.tag: (context) => LoginPage(),
            CameraPage.tag: (context) => CameraPage(),
            PainterPage.tag: (context) => PainterPage(),
            ClaimsPage.tag: (context) => ClaimsPage(),
            MapViewPage.tag: (context) => MapViewPage()
          },
        ),
  )
);
}

Future<void> runAppCenterServices() async {
  final ios = defaultTargetPlatform == TargetPlatform.iOS;
  var app_secret = ios ? "d8e13baf-0b8e-4270-8d82-4de97eacc657" : "e934b234-5b42-4253-a84e-acd40042ac9e";
  await AppCenter.start(app_secret, [AppCenterAnalytics.id, AppCenterCrashes.id]);
}