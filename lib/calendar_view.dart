import 'package:flutter/material.dart';

class Event {
  String time;
  String place;
  bool private;

  Event({this.time, this.place, this.private});
}


class CalendarView extends StatelessWidget {
  final events;

  CalendarView(this.events);

  @override
  Widget build(BuildContext context) {
    {
      return CustomScrollView(
        key: PageStorageKey(1),
        slivers: <Widget>[
          SliverOverlapInjector(
            handle: NestedScrollView
                .sliverOverlapAbsorberHandleFor(
                context),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 16.0,
            ),
//                              sliver: i.content
            sliver: SliverFixedExtentList(
              itemExtent: 130,
              delegate:
              SliverChildBuilderDelegate(
                    (BuildContext context,
                    int index) {
                  return Padding(
                      padding: const EdgeInsets
                          .symmetric(
                        vertical: 24.0,
                      ),
                      child: Card(child: ListTile(
                        isThreeLine: false,
                        dense: false,
                        leading: IconButton(
                            icon:
                            Icon(events[index].private ?Icons.calendar_today : Icons.report_problem),
                            color: Colors.lightBlue
                        ),
                        title: Text(
                            events[index].place),
                        subtitle: Text(
                            events[index]
                                .time),
                        trailing: IconButton(
                          icon:
                          Icon(Icons.explore),
                          color: Colors.lightBlue,
                        ),
                      )));
                },
                childCount: events.length,
              ),
            ),
          ),
        ],
      );
    }
  }
}