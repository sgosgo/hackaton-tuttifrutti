import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:login/claim_list.dart';
import 'calendar_view.dart';
import 'camera_page.dart';
import 'navigation.dart';
import 'state.dart';
import 'map_view_page.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Address {
  String street = '';
  String city = '';
  double latitude = 0;
  double longitude = 0;

  Address({this.street, this.city, this.longitude, this.latitude});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      city: json['city'],
      street: json['street'],
      latitude: json['latitude'],
      longitude: json['longitude'],
    );
  }
}

class Claim {
  int id = 0;
  String state = '';
  String company = '';
  bool hasTasks = false;
  bool hasMessages = false;
  Address address = new Address();

  Claim({this.id, this.company, this.state, this.address, this.hasTasks, this.hasMessages});

  factory Claim.fromJson(Map<String, dynamic> json) {
    return Claim(
        id: json['id'],
        state: json['stateName'],
        company: json['insuranceCompanyName'],
        hasTasks: json['hasTasks'],
        hasMessages: json['hasMessages'],
        address: new Address.fromJson(json["damageAddress"]));
  }
}

class ClaimsPage extends StatefulWidget {
  static String tag = 'claims-page';

  @override
  _ClaimsPageState createState() => new _ClaimsPageState();
}

class _ClaimsPageState extends State<ClaimsPage> with TickerProviderStateMixin {
  List<NavigationIconView> _navigationViews;
  int _currentIndex = 0;
  BottomNavigationBarType _type = BottomNavigationBarType.shifting;

  @override
  void initState() {
    super.initState();
    _navigationViews = <NavigationIconView>[
      NavigationIconView(
        icon: const Icon(Icons.view_list),
        title: 'Claims',
        color: Colors.blueGrey,
        vsync: this,
      ),
      NavigationIconView(
        activeIcon: const Icon(Icons.event_available),
        icon: const Icon(Icons.event_available),
        title: 'Calendar',
        color: Colors.blueGrey,
        vsync: this,
      ),
      NavigationIconView(
        activeIcon: const Icon(Icons.favorite),
        icon: const Icon(Icons.favorite_border),
        title: 'Tasks',
        color: Colors.blueGrey,
        vsync: this,
      ),
      NavigationIconView(
        activeIcon: const Icon(Icons.photo_album),
        icon: const Icon(Icons.photo_album),
        title: 'Photos',
        color: Colors.blueGrey,
        vsync: this,
      ),
    ];

    _navigationViews[_currentIndex].controller.value = 1.0;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<OurKurwaState, List<Claim>>(
        converter: (store) => store.state.claims.where((c) => c.hasMessages || c.hasTasks).toList()..addAll(store.state.claims.where((c) => !c.hasMessages && !c.hasTasks)),
        builder: (context, claims) {
          final events = claims != null && claims.length > 0 ? [
            new Event(place: '${claims[0].address.city} ${claims[0].address.street}', private: true, time: '2019-11-08 14:00 - 2019-11-08 14:00'),
            new Event(place: '${claims[1].address.city} ${claims[1].address.street}', private: true, time: '2019-11-08 17:00 - 2019-11-08 18:00'),
            new Event(place: '${claims[2].address.city} ${claims[2].address.street}', private: true, time: '2019-11-18 11:00 - 2019-11-18 14:00'),
            new Event(place: '${claims[3].address.city} ${claims[3].address.street}', private: true, time: '2019-11-18 15:00 - 2019-11-18 18:00'),
          ] : [];
          final items = [
            new TabItem(name: 'Claims', content: ClaimList(claims: claims)),
            new TabItem(name: 'Calendar', content: CalendarView(events)),
            new TabItem(name: 'Meanwhile', content: Container(child: Text('Coming soon'),))
          ]; 
          return DefaultTabController(
              length: items.length,
              child: Scaffold(
                floatingActionButton: FloatingActionButton(
                  tooltip: 'Take a photo',
                  child: Icon(Icons.camera),
                  onPressed: () {
                    Navigator.of(context).pushNamed(CameraPage.tag);
                  },
                ),
                body: NestedScrollView(
                  headerSliverBuilder:
                      (BuildContext context, bool innerBoxIsScrolled) {
                    return <Widget>[
                      SliverOverlapAbsorber(
                        handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                            context),
                        child: SliverAppBar(
                          title: const Text('Welcome!'),
                          pinned: true,
                          automaticallyImplyLeading: false,
                          expandedHeight: 100.0,
                          forceElevated: innerBoxIsScrolled,
                          bottom: TabBar(
                            tabs: items.map((i) => Tab(text: i.name)).toList(),
                          ),
                        ),
                      ),
                    ];
                  },
                  body: TabBarView(
                      children: items
                          .map((i) => SafeArea(
                                top: false,
                                bottom: false,
                                child: Builder(
                                  builder: (BuildContext context) {
//                        return i.content;
                                    return i.content;
                                  },
                                ),
                              ))
                          .toList()),
                ),
              ));
        });
  }
}


class TabItem {
  final String name;
  final Widget content;

  TabItem({this.name, this.content});
}
