import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'map_view_page.dart';

class ClaimList extends StatelessWidget {
  final claims;

  ClaimList({this.claims});

  @override
  Widget build(BuildContext context) {
    {
      return CustomScrollView(
        key: PageStorageKey(1),
        slivers: <Widget>[
          SliverOverlapInjector(
            handle: NestedScrollView
                .sliverOverlapAbsorberHandleFor(
                context),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 16.0,
            ),
//                              sliver: i.content
            sliver: SliverFixedExtentList(
              itemExtent: 130,
              delegate:
              SliverChildBuilderDelegate(
                    (BuildContext context,
                    int index) {
                  return Padding(
                      padding: const EdgeInsets
                          .symmetric(
                        vertical: 15.0,
                      ),
                      child: Card(child: ListTile(
                        isThreeLine: true,
                        dense: false,
                        leading: ExcludeSemantics(
                            child: CircleAvatar(
                              radius: 30,
                              backgroundImage: NetworkImage(
                                  "https://picsum.photos/200?random=$index"),
                              backgroundColor:
                              Colors.transparent,
                            )),
                        title: Text(
                            claims[index].state),
                        subtitle: Text((() {
                          if(claims[index].hasTasks){
                            return "${claims[index].company}\nTasks";}
                          return "${claims[index].company}";
                        })()),
                        trailing: IconButton(
                          icon:
                          Icon(Icons.explore),
                          color: Colors.lightBlue,
                          onPressed: () {
                            Navigator.of(context).pushNamed(
                                MapViewPage.tag,
                                arguments: MapViewArguments(
                                    LatLng(
                                        claims[index]
                                            .address
                                            .latitude,
                                        claims[index]
                                            .address
                                            .longitude),
                                    claims[index]
                                        .address
                                        .street));
                          },
                        ),
                      )
                      ));
                },
                childCount: claims.length,
              ),
            ),
          ),
        ],
      );
    }
  }
}